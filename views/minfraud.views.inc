<?php

/**
 * @file
 * Views hooks implemented for the Minfraud module.
 */

/**
 * Implements hook_views_data().
 */
function minfraud_views_data() {
  $data['minfraud'] = [
    'table' => [
      'group' => t('minfraud'),
      'base' => [
        'field' => 'mfid',
        'title' => t('Minfraud'),
        'help' => t('Minfrauds entries'),
        'weight' => -10,
      ],
    ],
    'mfid' => [
      'title' => t('Minfraud ID'),
    ],
    'score' => [
      'title' => t('Score'),
      'help' => t('User score'),
      'field' => [
        'handler' => 'views_handler_field_numeric',
      ],
      'filter' => [
        'handler' => 'views_handler_filter_numeric',
      ],
      'argument' => [
        'handler' => 'views_handler_argument_numeric',
      ],
      'sort' => [
        'handler' => 'views_handler_sort',
      ],
    ],
    'country' => [
      'title' => t('Country'),
      'help' => t('User country'),
      'field' => [
        'handler' => 'views_handler_field',
      ],
      'filter' => [
        'handler' => 'views_handler_filter_string',
      ],
      'argument' => [
        'handler' => 'views_handler_argument_string',
      ],
      'sort' => [
        'handler' => 'views_handler_sort',
      ],
    ],

    'uid' => [
      'title' => t('User Reference'),
      'help' => t('Adds a reference to users'),
      'relationship' => [
        'base' => 'users',
        'base field' => 'uid',
        'field' => 'uid',
        'handler' => 'views_handler_relationship',
        'label' => t('Relationship with user'),
        'title' => t('User relationship'),
        'help' => t('More information about relationship.'),
      ],
    ],
    'hostname' => [
      'title' => t('Ip address'),
      'help' => t('User ip address when registering'),
      'field' => [
        'handler' => 'views_handler_field',
      ],
      'filter' => [
        'handler' => 'views_handler_filter_string',
      ],
      'argument' => [
        'handler' => 'views_handler_argument_string',
      ],
      'sort' => [
        'handler' => 'views_handler_sort',
      ],
    ],
  ];
  return $data;
}
