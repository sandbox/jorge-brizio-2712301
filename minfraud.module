<?php

/**
 * @file
 * Fetchs information about your users, it contacts Maxmind service.
 *
 * This module will send users IP address to MaxMind service, they will return
 * information about user location, if users uses a proxy.
 * In order to use this, you will need an API Key from MaxMind.
 *
 * @author Jorge Manuel Alves.
 */

module_load_include('inc', 'minfraud', 'includes/minfraud');

define('MINFRAUD_GEOPIP', 'GEOIP');
define('MINFRAUD_PD', 'PD');
define('MINFRAUD', 'minfraud');

/**
 * Implements hook_help().
 */
function minfraud_help($path, $arg) {
  switch ($path) {
    case 'admin/help#minfraud':
      return '<p>' . t("Description must be here...") . '</p>';
  }
}

/**
 * Implements hook_permission().
 */
function minfraud_permission() {
  return [
    'administer minfraud' => [
      'title' => t('Administer MinFraud'),
    ],
    'view minfraud' => [
      'title' => t('View Minfraud results'),
    ],
  ];
}

/**
 * Implements hook_menu().
 */
function minfraud_menu() {
  $items = [];

  $items['admin/config/people/minfraud'] = [
    'title' => 'Minfraud - Maxmind',
    'description' => 'Configure the Minfraud settings',
    'access arguments' => ['adminster minfraud'],
    'page callback' => 'drupal_get_form',
    'page arguments' => ['minfraud_admin_settings'],
    'file' => 'includes/minfraud.admin.inc',
  ];

  $items['user/%user/minfraud'] = [
    'title' => 'Minfraud',
    'page callback' => 'drupal_get_form',
    'page arguments' => ['minfraud_users_admin_form', 1],
    'access arguments' => ['view minfraud'],
    'type' => MENU_LOCAL_TASK,
    'file' => 'includes/minfraud.admin.inc',
  ];

  return $items;
}

/**
 * Get a Minfraud API object for communication with the Maxmind service.
 *
 * @return Minfraud
 *   It will return an instance of the Minfruad.
 */
function minfraud_get_api_object() {
  $minfraud = &drupal_static(__FUNCTION__);
  if (isset($minfraud)) {
    return $minfraud;
  }
  // Load Maxmind Api key.
  $minfraud_api_key = variable_get('minfraud_api_key', '');

  if ($minfraud_api_key) {
    $minfraud = new minfraud($minfraud_api_key);
    return $minfraud;
  }
  else {
    watchdog(MINFRAUD, 'Missing MaxMind API key.', WATCHDOG_CRITICAL);
  }
  return NULL;
}

/**
 * Implements hook_user_insert().
 */
function minfraud_user_insert(&$edit, $account, $category) {
  if (variable_get('minfraud_user_register', TRUE)) {
    minfraud_proc_user($account->uid);
  }
}

/**
 * Implements hook_user_login().
 *
 * Detects IP and maxmind data upon user login.
 */
function minfraud_user_login(&$edit, $account) {
  $roles = variable_get('minfraud_user_roles');
  if ($roles) {
    $roles = array_filter($roles);
    // Bypass if user has a role not to be tracked.
    if (array_intersect_key($roles, $account->roles)) {
      return;
    }
    // Bypass if user is not to be tracked.
    if (isset($account->data['minfraud_donttrack'])) {
      return;
    }
    minfraud_proc_user($account->uid);
  }

}

/**
 * Loads minfraud object for further request processing.
 *
 * @param int $uid
 *    User id.
 */
function minfraud_proc_user($uid) {
  try {
    $minfraud = minfraud_get_api_object();
    if (!$minfraud) {
      throw new MinfraudException('Cannot use Minfraud without an API key.');
    }
    minfraud_loguser($minfraud, $uid);
  } catch (Exception $e) {
    watchdog('minfraud', 'An error occurred requesting Maxmind API. "%message"', [
      '%message' => $e->getMessage(),
    ], WATCHDOG_ERROR);
  }
}

/**
 * Records Maxmind results to minfraud table.
 *
 * @param object $minfraud
 *   Minifraud object.
 * @param int $uid
 *    User id.
 */
function minfraud_loguser($minfraud, $uid) {
  // What type of service to make the request.
  $services = variable_get('minfraud_maxmind_services');
  $user_ip = ip_address();
  // @todo: Bypass localhost ip addresses.
  $table = MINFRAUD;
  $record = new stdClass();
  // Get proxy detection and country values.
  if ($services[MINFRAUD_PD]) {
    $report = $minfraud->doProxyDetectionQuery($user_ip);
    $record->score = ($report) ? $report : t('N/A');
  }

  if ($services[MINFRAUD_GEOPIP]) {
    $country = $minfraud->getCountry($user_ip);
    if ($country && !(strpos($country, 'NOT_FOUND'))) {
      $record->country = $country;
    }
    else {
      $record->country = t('N/A');
    }
  }

  $record->uid = $uid;
  $record->hostname = $user_ip;
  $record->login = REQUEST_TIME;
  drupal_write_record($table, $record);
  watchdog('Minfraud', 'Added user with score %score', ['%score' => $record->score]);
}

/**
 * Sets per user a track or not to track possibility.
 *
 * @param int $uid
 *   The user.
 * @param int $value
 *   Track or not to track value.
 *
 * @throws \Exception
 */
function minfraud_user_track($uid, $value) {
  $user_obj = user_load($uid);
  if (!$value) {
    $minfraud_data = [
      'minfraud_donttrack' => 1,
    ];
  }
  else {
    $minfraud_data = [];
    unset($user_obj->data['minfraud_donttrack']);
  }
  user_save($user_obj, ['data' => $minfraud_data]);
}

/**
 * Implements hook_page_alter().
 *
 * Insert JavaScript to the chosen scope/region of the page.
 */
function minfraud_page_alter(&$page) {
  // We allow different scopes. Default to 'header'
  // but allow user to override if they really need to.
  $scope = variable_get('minfraud_js_scope', 'header');
  $minfraud_maxmind_username = variable_get('minfraud_maxmind_username', '');
  if ($minfraud_maxmind_username && variable_get('minfraud_device_tracking', '')) {
    $script = "maxmind_user_id = \"$minfraud_maxmind_username\"";
    $script .= "(function() {";
    $script .= "var loadDeviceJs = function() {";
    $script .= "var element = document.createElement('script');";
    $script .= "element.src = ('https:' == document.location.protocol ? 'https:' : 'http:')";
    $script .= "+ '//device.maxmind.com/js/device.js';)";
    $script .= "document.body.appendChild(element);";
    $script .= "};";
    $script .= "if (window.addEventListener) {";
    $script .= "window.addEventListener('load', loadDeviceJs, false);";
    $script .= "} else if (window.attachEvent) {";
    $script .= "window.attachEvent('onload', loadDeviceJs);";
    $script .= "}";
    $script .= "})();";
    drupal_add_js($script, ['scope' => $scope, 'type' => 'inline']);
  }
}

/**
 * Generates a table with all the user minfraud history.
 *
 * @param object $account
 *    The user account.
 *
 * @return string
 *    Returns rendered table.
 *
 * @throws \Exception
 */
function minfraud_userresultstable($account) {
  // Define table headers.
  $header = [
    t('Login'),
    t('Score'),
    t('Country'),
    t('Hostname'),
  ];
  $select = db_select('minfraud', 'm')
    ->fields('m', ['login', 'score', 'country', 'hostname'])
    ->condition('m.uid', $account->uid, '=')
    ->orderBy('mfid', 'DESC');;
  // Get the object.
  $results = $select->execute()->fetchAll();
  $out = '';
  if ($results) {
    foreach ($results as $result) {
      $rows[] = [
        date('d M Y h:m:s', $result->login),
        $result->score,
        $result->country,
        $result->hostname,
      ];
    }
    if (isset($rows)) {
      $per_page = 30;
      $current_page = pager_default_initialize(count($rows), $per_page);
      $chunks = array_chunk($rows, $per_page, TRUE);
      $out = theme('table', [
        'header' => $header,
        'rows' => $chunks[$current_page],
      ]);
      $out .= theme('pager', ['quantity', count($rows)]);
    }
  }
  else {
    drupal_set_message(t('No results found for this user.'));
  }
  return $out;
}

/**
 * Implements hook_user_cancel().
 */
function minfraud_user_cancel($edit, $account, $method) {
  if (variable_get('minfraud_maint') == 'cancel_user') {
    _minfraud_user_delete_helper($account);
  }
}

/**
 * Implements hook_user_delete().
 */
function minfraud_user_delete($account) {
  if (variable_get('minfraud_maint') == 'delete_user') {
    _minfraud_user_delete_helper($account);
  }
}

/**
 * Helper for deleting user.
 *
 * @param object $account
 *   Account object.
 */
function _minfraud_user_delete_helper($account) {
  db_delete(MINFRAUD)
    ->condition('uid', $account->uid)
    ->execute();
}

/**
 * Implements hook_views_api().
 */
function minfraud_views_api() {
  return [
    'api' => 3.0,
    'path' => drupal_get_path('module', 'minfraud') . '/views',
  ];
}
