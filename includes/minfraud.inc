<?php

/**
 * @file
 * Minfraud class object.
 */

/**
 * Class Minfraud.
 */
class Minfraud {

  protected $GeoQuery;
  protected $ProxQuery;
  protected $QueryIp;
  protected $QueryResult;
  protected $ServiceSite;

  /**
   * Minfraud constructor.
   *
   * @param string $apikey
   *   Maxmind api key.
   */
  public function __construct($apikey) {

    if (!$apikey) {
      throw new MinifraudException(t('You must provide a MinFraud API key'));
    }
    $this->ServiceSite = 'maxmind.com';
    $this->ProxQuery = "https://minfraud.maxmind.com/app/ipauth_http?l=" . $apikey . "&i=";
    $this->GeoQuery = "http://geoip3.maxmind.com/a?l=" . $apikey . "&i=";
  }

  /**
   * Method for getting Proxy detection. This method gets the score by service.
   */
  public function doProxyDetectionQuery($ip) {
    $this->QueryResult = drupal_http_request($this->ProxQuery . $ip);
    return str_replace('proxyScore=', '', self::resultRet());
  }

  /**
   * Method for getting light Geo Query. This method gets the country.
   */
  public function doGeoQuery($ip) {
    return $this->GeoQuery . $ip;
  }

  /**
   * Checks if Service is alive.
   */
  public function getServiceStatus() {
    if ($this->QueryResult->code
      && $this->QueryResult->code == 200
    ) {
      return TRUE;
    }
    watchdog(
      'minfraud', 'The web service request from @site seems to be broken, due to @errorcode with @errormsg.',
      [
        '@site' => $this->ServiceSite,
        '@errorcode' => $this->QueryResult->code,
        '@errormesg' => $this->QueryResult->error,
      ], WATCHDOG_WARNING
    );
    return FALSE;
  }

  /**
   * Method that returns the service data.
   */
  private function resultRet() {
    if (self::getServiceStatus()) {
      return $this->QueryResult->data;
    }
    else {
      watchdog(
        'minfraud', 'The web service request from @site seems to be broken, due to @errorcode with @errormsg.',
        [
          '@site' => $this->ServiceSite,
          '@errorcode' => $this->QueryResult->code,
          '@errormesg' => $this->QueryResult->error,
        ], WATCHDOG_WARNING
      );
    }
  }

  /**
   * Gets the Country based on IP.
   *
   * @param string $ip
   *    Ip of the user.
   *
   * @return mixed
   *    Returns country 2-letter code or false if not found.
   */
  public function getCountry($ip) {

    $url = parse_url(self::doGeoQuery($ip));
    $host = $url["host"];
    $path = $url["path"] . "?" . $url["query"];
    $timeout = 1;
    $errno = NULL;
    $errstr = NULL;
    $buf = '';
    try {
      $fp = @fsockopen($host, 80, $errno, $errstr, $timeout);
      if (!$fp) {
        throw new MinfraudException('Unable to connect . ' . $errno . ' - ' . $errstr);
      }
      if ($fp) {
        fwrite($fp, "GET $path HTTP/1.0\nHost: " . $host . "\n\n");
        while (!feof($fp)) {
          $buf .= fgets($fp, 128);
        }
        $lines = explode("\n", $buf);
        $country = $lines[count($lines) - 1];
        fclose($fp);
        return $country;
      }
    }
    catch (Exception $e) {
      watchdog('minfraud', 'An error occurred requesting Maxmind API. "%message"', [
        '%message' => $e->getMessage(),
      ], WATCHDOG_ERROR);
    }
  }

}
