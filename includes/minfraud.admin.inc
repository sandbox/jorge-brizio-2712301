<?php

/**
 * @file
 * Admin interface callbacks/handlers to configure minfraud.
 */

/**
 * Return minfraud global settings form.
 */
function minfraud_admin_settings($form, &$form_state) {

  $minfraud_services = [
    'GEOIP' => t('Minfraud - GeoIP'),
    'PD' => t('Proxy Detection'),
  ];

  $minfraud_options = [
    'minfraud_user_register' => t('Registration'),
    'minfraud_user_login' => t('Log in'),
  ];

  $roles = user_roles();
  $minfraud_roles = [];
  foreach ($roles  as $role) {
    $minfraud_roles[$role] = $role;
  }
  $form['minfraud_maxmind'] = [
    '#type' => 'fieldset',
    '#title' => t('MaxMind Api'),
    '#description' => t('Settings'),
  ];

  $form['minfraud_maxmind']['minfraud_maxmind_services'] = [
    '#type' => 'checkboxes',
    '#options' => $minfraud_services,
    '#title' => t('Choose your service(s):'),
    '#default_value' => variable_get('minfraud_maxmind_services', ''),
  ];

  $form['minfraud_maxmind']['minfraud_api_key'] = [
    '#type' => 'textfield',
    '#title' => t('MinFraud license key'),
    '#description' => t('Insert your minfraud Api license key.'),
    '#default_value' => variable_get('minfraud_api_key', ''),
    '#required' => TRUE,
  ];

  $form['minfraud_settings'] = [
    '#type' => 'fieldset',
    '#title' => t('Settings'),
    '#description' => t('Global minfraud settings.'),
    '#required' => TRUE,
  ];

  $form['minfraud_settings']['minfraud_user_register'] = [
    '#type' => 'checkboxes',
    '#title' => t('Used in'),
    '#options' => $minfraud_options,
    '#description' => t('Where minfraud should act.'),
    '#required' => TRUE,
    '#default_value' => variable_get('minfraud_user_register', ''),
  ];

  $form['minfraud_settings']['minfraud_user_roles'] = [
    '#type' => 'checkboxes',
    '#title' => t('User roles'),
    '#options' => $minfraud_roles,
    '#description' => t('Select which user roles minfraud should act.'),
    '#required' => TRUE,
    '#default_value' => variable_get('minfraud_user_roles', ''),
  ];

  $form['minfraud_settings']['minfraud_device_tracking'] = [
    '#type' => 'checkbox',
    '#title' => t('Device Tracking Add-on'),
    '#description' => t(
        'The device tracking add-on for the minFraud and Proxy Detection
    services identifies devices as they move across networks and enhances the ability of
    the minFraud and Proxy Detection services to detect fraud.
    If a fraudster changes proxies while they are browsing your website or between visits
    to your website, you will observe an increased proxyScore and/or riskScore in the
    minFraud and Proxy Detection output associated with their transactions.'
    ),
    '#default_value' => variable_get('minfraud_device_tracking', ''),
  ];

  $form['minfraud_settings']['minfraud_js_scope'] = [
    '#type' => 'select',
    '#title' => t('JavaScript scope'),
    '#description' => t('Minfraud recommends adding the external JavaScript to the header for performance reasons.'),
    '#options' => [
      'footer' => t('Footer'),
      'header' => t('Header'),
    ],
    '#default_value' => variable_get('minfraud_js_scope', ''),
  ];

  $options = [
    'delete_user' => t('Delete minfraud data belonging to a user, when that user is deleted.'),
    'cancel_user' => t('Delete minfraud data belonging to a user, when that user is blocked.'),
    'never' => t('Never delete user data. Only when module will be disabled.'),
  ];

  $form['minfraud_settings']['minfraud_maint'] = [
    '#type' => 'radios',
    '#title' => t('When should minfraud user data be delete?'),
    '#description' => t(
        'For performance and good practise reason, you should
    delete user data either when canceling the account or when deleting a user.'
    ),
    '#options' => $options,
    '#default_value' => variable_get('minfraud_maint', ''),
  ];
  return system_settings_form($form);
}

/**
 * Minfraud user administration form.
 */
function minfraud_users_admin_form($form, &$form_state, $account) {
  $form['minfraud_userview'] = [
    '#type' => 'fieldset',
    '#title' => t('Minfraud user information'),
    '#description' => t('Information about the current user.'),
  ];

  $form['minfraud_userview']['#suffix'] = minfraud_userresultstable($account);
  $track = 1;
  if (isset($account->data['minfraud_donttrack'])) {
    $track = 0;
  }

  $form['minfraud_userview']['minfraud_user_track'] = [
    '#type' => 'radios',
    '#options' => [
      0 => t("Don't track user"),
      1 => t('Keep tracking.'),
    ],
    '#title' => t('User tracking:'),
    '#default_value' => $track,
    '#required' => TRUE,
  ];

  // Passing the user.
  $form['#user_uid'] = $account->uid;

  $form['minfraud_userview']['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Submit',
  );
  return $form;
}

/**
 * Submit handler for users tracking form.
 */
function minfraud_users_admin_form_submit($form, &$form_state) {
  minfraud_user_track($form['#user_uid'], $form_state['values']['minfraud_user_track']);
}
